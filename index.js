//canvas
const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

ctx.strokeStyle = '#000000'; //initial color
ctx.fillStyle = "white";
ctx.lineJoin = 'round'; 
ctx.lineCap = 'round';
ctx.lineWidth = 10;

var dragStartLocation;
let nowDrawing = false;
let nowDragging = false;
let lastX = 0;
let lastY = 0;
let hue = 0;
let mode = "pen"; // pen, eraser, text, shape
let shape_type = "unknown";
var InputExist = false;
var rainbowOn = false;

let pen = document.getElementById("pen");
let era = document.getElementById("era");
let undo = document.getElementById("undo");
let redo = document.getElementById("redo");
let text = document.getElementById("text");
let fullshape = document.getElementById("fullshape");
let rainbow = document.getElementById("rainbow");

// STATE ************************************************
var snapshot;
function saveState(){
  let state = ctx.getImageData(0, 0, canvas.width, canvas.height);
  window.history.pushState(state, null);
}

function takeSnapshot() {
  snapshot = ctx.getImageData(0, 0, canvas.width, canvas.height);
}

function restoreSnapshot() {
  ctx.putImageData(snapshot, 0, 0);
}
// get initial state (for redo)
saveState();
// undo redo
undo.addEventListener('click',function(){ window.history.go(-1); },false);
redo.addEventListener('click',function(){ window.history.go(1); },false);
window.addEventListener('popstate', changeStep, false);
function changeStep(e){
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  // change to specific state
  if( e.state ){
    ctx.putImageData(e.state, 0, 0);
  }
}
// STATE end ------------------------------------------------

// btns
function control(val){
  if(val == "era"){
    mode = "eraser";
    canvas.classList.remove("shape-cursor");
    canvas.classList.remove("pen-cursor");
    canvas.classList.remove("text-cursor");
    era.classList.add("era-cursor");
    canvas.classList.add("era-cursor");
  }
  if(val == "pen"){
    mode = "pen";
    canvas.classList.remove("shape-cursor");
    canvas.classList.remove("era-cursor");
    canvas.classList.remove("text-cursor");
    pen.classList.add("pen-cursor");
    canvas.classList.add("pen-cursor");
  }
  if(val == "clean"){
    if (confirm("Are you sure to clean?")) {
      ctx.clearRect(0, 0, canvas.width, canvas.height);
    }
  }
  if(val == "t"){
    mode = "text";
    canvas.classList.remove("shape-cursor");
    canvas.classList.remove("era-cursor");
    canvas.classList.remove("pen-cursor");
    text.classList.add("text-cursor");
    canvas.classList.add("text-cursor");
  }
  if(val == "circle"){
    mode = "shape";
    shape_type = "circle";
    canvas.classList.remove("era-cursor");
    canvas.classList.remove("pen-cursor");
    canvas.classList.remove("text-cursor");
    canvas.classList.add("shape-cursor");
  }
  if(val == "rect"){
    mode = "shape";
    shape_type = "rect";
    canvas.classList.remove("era-cursor");
    canvas.classList.remove("pen-cursor");
    canvas.classList.remove("text-cursor");
    canvas.classList.add("shape-cursor");
  }
  if(val == "tri"){
    mode = "shape";
    shape_type = "tri";
    canvas.classList.remove("era-cursor");
    canvas.classList.remove("pen-cursor");
    canvas.classList.remove("text-cursor");
    canvas.classList.add("shape-cursor");
  }
  else return;
}
// TEXT ************************************************
// text , add input box
function addInput(x, y) {
  var input = document.createElement('input');
  input.style.left = x + 'px';
  input.style.top = y + 'px';
  input.type = 'text';
  input.style.position = 'fixed';
  // call functino that deal with enter
  input.onkeydown = pressEnter;
  document.body.appendChild(input);
  input.focus();
  InputExist = true;
}
// remove textbox when press enter
function pressEnter(e) {
  if (e.key == "Enter") {
      drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
      // remove input from html
      document.body.removeChild(this);
      InputExist = false;
  }
}
// Draw the text onto canvas:
function drawText(txt, x, y) {
  var fontsize_select = document.getElementById("text-fontsize").value;
  var fontstyle_select = document.getElementById("text-fonttype").value;
  ctx.fillStyle = document.getElementById("colorpicker").value; //important
  ctx.font = fontsize_select + 'px ' + fontstyle_select;
  ctx.textBaseline = 'top';
  ctx.textAlign = 'left';
  // console.log(txt, x, y, ctx.font );
  ctx.fillText(txt, x, y);
  //save the state when finish texting
  saveState();
}
// TEXT end ------------------------------------------------



// DRAW ****************************************************
function draw(e) {
  if(!nowDrawing) {
    return;
  }
  if(!canvas.getContext){
    // un-support
    alert(" canvas-unsupported :(( ");
    return;
  }

  if(nowDragging){
    var position;
    restoreSnapshot();
    position = getClientPosition(e);
    ctx.globalCompositeOperation = "source-over";
    if(shape_type == "circle")
      drawCircle(position);
    if(shape_type == "rect")
      drawRect(position);
    if(shape_type == "tri")
      drawTri(position);
    if(shape_type == "unknown")
      return;
  }
  // deal with changing color/brush size
  if(rainbow.checked){
    ctx.strokeStyle = `hsl(${hue}, 100%, 50%)`;
  }
  else ctx.strokeStyle = document.getElementById("colorpicker").value;
  ctx.lineWidth = document.getElementById("slider").value;
  // stroke is inside or the text would also stroke
  if(mode == "pen"){
    hue++;
    if(hue >= 360) { hue = 0 }
    ctx.beginPath();
    ctx.moveTo(lastX, lastY);
    ctx.lineTo(e.offsetX, e.offsetY);
    ctx.globalCompositeOperation = "source-over";
    ctx.stroke();
  }
  if(mode == "eraser"){
    ctx.beginPath();
    ctx.moveTo(lastX, lastY);
    ctx.lineTo(e.offsetX, e.offsetY);
    ctx.globalCompositeOperation="destination-out";
    ctx.stroke();
  }

  [lastX, lastY] = [e.offsetX, e.offsetY];	
}
// stopdraw
function stopdraw(e){
  if(mode == "shape"){
    nowDragging = false;
    restoreSnapshot();
    var position = getClientPosition(e);
    if(shape_type == "circle")
      drawCircle(position);
    if(shape_type == "rect")
      drawRect(position);
    if(shape_type == "tri")
      drawTri(position);
    // console.log("draw shape done");
    saveState();
  }
  else if(mode == "text"){
    ctx.globalCompositeOperation = "source-over";
    if(InputExist)return;
    addInput(e.clientX, e.clientY);
  }
  else{
      saveState();
  }
  nowDrawing = false;
}
function startdraw(e){
  if(mode== "text")
    nowDrawing = false;
  else nowDrawing = true;
  [lastX, lastY] = [e.offsetX, e.offsetY];
  // cursor of first draw
  if(mode == "pen")
    canvas.classList.add("pen-cursor");
  if(mode == "eraser")
    canvas.classList.add("era-cursor");
  if(mode == "shape"){
    nowDragging = true;
    dragStartLocation = getClientPosition(e);
    takeSnapshot();
  }
}
canvas.addEventListener('mousedown', startdraw);
canvas.addEventListener('mousemove', draw);	
canvas.addEventListener('mouseup', stopdraw);
canvas.addEventListener('mouseout', () => nowDrawing = false);
// DRAW end ------------------------------------------------



// IMAGE ****************************************************
// upload image
let imgInput = document.getElementById('imageInput');
imgInput.addEventListener('change', function(e) {
  // if the input change, do below
  if(e.target.files) {
    // e.target.file is an array, img is at [0]
    let imgfile = e.target.files[0];
    var reader  = new FileReader();
    reader.readAsDataURL(imgfile); 
    // make sure it's loaded
    reader.onloadend = function (e) {
      var myImage = new Image(); // Creates image object
      myImage.src = e.target.result; // Assigns converted image to image object
      myImage.onload = function(ev) {
        // resize img if it's bigger than canvas
        var temp_canvas = document.getElementById("canvas");
        var temp_ctx = canvas.getContext('2d');
        if(myImage.height > temp_canvas.height || myImage.width > temp_canvas.width){
          temp_canvas.height = myImage.height * 0.5;
          temp_canvas.width = myImage.width * 0.5;
        }
        else{
          temp_canvas.height = myImage.height;
          temp_canvas.width = myImage.width;
        }
        temp_ctx.drawImage(myImage,0,0,temp_canvas.width,temp_canvas.height);
        ctx.drawImage(temp_canvas,0,0); 
      }
    }
 }
});
// download
let download = document.getElementById('download');
download.addEventListener("click", function(e) {
  var dataURL = canvas.toDataURL("image/png", 1.0);
  var date = new Date();
  // define file name with date
  var file_name = (date.getFullYear()).toString() + "" + (date.getMonth()).toString() + "" + (date.getDate()).toString() + "-" + (date.getHours()).toString() + (date.getMinutes()).toString();
  downloadImage(dataURL, file_name+'-canvas.png');
});
// create <a> and do download
function downloadImage(data, filename = 'untitled.png') {
  var a = document.createElement('a');
  a.href = data;
  a.download = filename;
  document.body.appendChild(a);
  a.click();
}
// IMAGE end ------------------------------------------------



// SHAPE ****************************************************
function drawCircle(position) {
  // console.log("hi");
  ctx.strokeStyle = document.getElementById("colorpicker").value;
  ctx.fillStyle  = document.getElementById("colorpicker").value;
  ctx.lineWidth = document.getElementById("slider").value;
  var radius = Math.sqrt(Math.pow((dragStartLocation.x - position.x), 2) + Math.pow((dragStartLocation.y - position.y), 2));
  ctx.beginPath();
  ctx.arc(dragStartLocation.x, dragStartLocation.y, radius, 0, 2 * Math.PI, false);
  if(fullshape.checked)
    ctx.fill();
  else ctx.stroke();
}

function drawRect(position){
  ctx.strokeStyle = document.getElementById("colorpicker").value;
  ctx.fillStyle  = document.getElementById("colorpicker").value;
  ctx.lineWidth = document.getElementById("slider").value;
  ctx.beginPath();
  ctx.rect(position.x, position.y, dragStartLocation.x - position.x, dragStartLocation.y - position.y);
  if(fullshape.checked)
    ctx.fill();
  else ctx.stroke();
}

function drawTri(position){
  ctx.strokeStyle = document.getElementById("colorpicker").value;
  ctx.fillStyle  = document.getElementById("colorpicker").value;
  ctx.lineWidth = document.getElementById("slider").value;
  ctx.beginPath();
  ctx.moveTo(position.x, dragStartLocation.y);
  ctx.lineTo((dragStartLocation.x+position.x)/2, position.y);
  ctx.lineTo(dragStartLocation.x, dragStartLocation.y);
  ctx.closePath();
  if(fullshape.checked)
    ctx.fill();
  else ctx.stroke();
}
function getClientPosition(event) {
  var x = event.clientX;
  var y = event.clientY;

  return {x: x, y: y};
}
// SHAPE end ------------------------------------------------
