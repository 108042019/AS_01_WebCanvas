# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Fill the shape                                   | 1~5%      | Y         |
| Rainbow pen                                      | 1~5%      | Y         |

---

### How to use 
#### Overview
![](https://i.imgur.com/uI8mxIE.png)

#### 1. Basic
- ![](https://i.imgur.com/E0lCSPR.png) pencil : mousedown and draw on canvas.
- ![](https://i.imgur.com/cWQj7AD.png) eraser : mousedown and eraser what's on canvas.
- ![](https://i.imgur.com/PYFCvyO.png) text : click on where you want to put your text, text them in input box and press "enter" when you are done, then it will show on the canvas.
![](https://i.imgur.com/h59gF6k.png)
![](https://i.imgur.com/G6bHyv4.png)

#### 2. Shape
- ![](https://i.imgur.com/P0PB1KY.png) shape : click on the button of shape you want, drag it on canvas,the shape will appear on canvas when mouseup
- ![](https://i.imgur.com/NZP5Kq4.png)
you can choose "fill the shape / just draw the margin" in the custom section !
![](https://i.imgur.com/h8rRMNX.png)
![](https://i.imgur.com/11YUx5U.png)

#### 3. Custom
- ![](https://i.imgur.com/haUrV7v.png)
drag the scroll bar to change brush size
![](https://i.imgur.com/P2CDX8P.png)

- ![](https://i.imgur.com/f2C1yb9.png) \
choose your color of bruch/shape, font size and font type\
![](https://i.imgur.com/maxVuKN.jpg)

#### 4. File
##### Download
- click Download to download what you draw to your computer!
- file name will be "YearMonthDate-HourMinute-canvas.png"
![](https://i.imgur.com/HnJqvQX.jpg) 
##### Upload
- click upload and import photo you want onto the canvas.
![](https://i.imgur.com/nHuGkSp.png)
   

### Function description
1. Fill the shape
![](https://i.imgur.com/NZP5Kq4.png)
- fill the shape checked : your shape is filled with color of color picker.
- fill the shape unchecked : your shape is draw with only frame, with color of color picker.\
(demonstrated in the 2. Shape section)

2. Rainbow pen
- checked the checkbox to turn on Rainbow pen
![](https://i.imgur.com/uI8mxIE.png)

### Gitlab page link
https://108042019.gitlab.io/AS_01_WebCanvas

### Others
none
